<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Category;
use App\Form\CategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CategoryController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }   
    /**
     * @Route("/category/liste", name="category_list")
     */
    public function index(): Response
    {
        return $this->render('category/index.html.twig', [
            'categories' => $this->manager->getRepository(Category::class)
            ->findAll()        
        ]);
    }

    /**
     * @Route("/category/new", name="category_new")
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category=$form->getData();
            $this->manager->persist($category);
            $this->manager->flush();

            return $this->redirectToRoute('category_list');

        }

        return $this->render('Category/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/category/edit$/{id},", name="category_edit")
     */
    public function editAction($id, Request $request)
    {
        $category = $this->manager->getRepository(Category::class)->findOneBy(['id'=>$id]);

        if (!$category instanceof Category){
            throw new NotFoundHttpException();
        }
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryForm=$form->getData();
            $category->setName($categoryForm->getName());

            $this->manager->flush();

            return $this->redirectToRoute('category_list');

        }

        return $this->render('Category/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/category/delete/{id}", name="category_delete")
     */
    public function deleteAction($id)
    {
        $category = $this->manager->getRepository(Category::class)->findOneBy(['id'=>$id]);

        if (!$category instanceof Category){
            throw new NotFoundHttpException();
        }
        $this->manager->remove($category);
        $this->manager->flush();

        return $this->redirectToRoute('category_list');
    }
}
