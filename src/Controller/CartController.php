<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Form\CartType;
use App\Repository\CartRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\ORM\EntityManagerInterface;



class CartController extends AbstractController
{
     /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }   
    /**
     * @Route("/cart/list", name="cart_list")
     */
    public function index(): Response
    {
        return $this->render('cart/index.html.twig', [
            'cart' => $this->manager->getRepository(Cart::class)
                ->findAll()
        ]);
    }

     /**
     * @Route("/cart/new", name="cart_new")
     */
    public function newAction(Request $request)
    {
        $cart = new Cart();
        $form = $this->createForm(CartType::class, $cart);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cart=$form->getData();
            $this->manager->persist($cart);
            $this->manager->flush();

            return $this->redirectToRoute('cart_list');

        }

        return $this->render('cart/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/cart/edit$/{id},", name="cart_edit")
     */
    public function editAction($id, Request $request)
    {
        $cart = $this->manager->getRepository(Cart::class)->findOneBy(['id'=>$id]);

        if (!$cart instanceof Cart){
            throw new NotFoundHttpException();
        }
        $form = $this->createForm(CartType::class, $cart);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cartForm=$form->getData();
            $cart->setName($cartForm->getName());

            $this->manager->flush();

            return $this->redirectToRoute('cart_list');

        }

        return $this->render('Cart/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/cart/delete/{id}", name="cart_delete")
     */
    public function deleteAction($id)
    {
        $cart = $this->manager->getRepository(Cart::class)->findOneBy(['id'=>$id]);

        if (!$cart instanceof Cart){
            throw new NotFoundHttpException();
        }
        $this->manager->remove($cart);
        $this->manager->flush();

        return $this->redirectToRoute('cart_list');
    }
}
