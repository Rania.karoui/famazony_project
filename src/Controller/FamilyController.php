<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Family;
use App\Form\FamilyType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class FamilyController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }   
    
    /**
     * @Route("/family/liste", name="family_list")
     */
    public function index(): Response
    {
        return $this->render('family/index.html.twig', [
            'families' => $this->manager->getRepository(Family::class)
                ->findAll()
        ]);
        
    }

    /**
     * @Route("/family/new", name="family_new")
     */
    public function newAction(Request $request)
    {
        $family = new Family();
        $form = $this->createForm(FamilyType::class, $family);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $family=$form->getData();
            $this->manager->persist($family);
            $this->manager->flush();

            return $this->redirectToRoute('family_list');

        }

        return $this->render('Family/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/family/edit$/{id},", name="family_edit")
     */
    public function editAction($id, Request $request)
    {
        $family = $this->manager->getRepository(Family::class)->findOneBy(['id'=>$id]);

        if (!$family instanceof Family){
            throw new NotFoundHttpException();
        }
        $form = $this->createForm(FamilyType::class, $family);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $familyForm=$form->getData();
            $family->setName($familyForm->getName());

            $this->manager->flush();

            return $this->redirectToRoute('family_list');

        }

        return $this->render('Family/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/family/delete/{id}", name="family_delete")
     */
    public function deleteAction($id)
    {
        $family = $this->manager->getRepository(Family::class)->findOneBy(['id'=>$id]);

        if (!$family instanceof Family){
            throw new NotFoundHttpException();
        }
        $this->manager->remove($family);
        $this->manager->flush();

        return $this->redirectToRoute('family_list');
    }
    
    
}
