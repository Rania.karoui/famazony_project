<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class BaseController
 */
class BaseController extends AbstractController
{
    /**
     * @Route("/", name="famazony_homepage")
     */
    public function indexAction()
    {
        return $this->render('homepage.html.twig');
    }

    /**
     * @Route("/home-clientel", name="home_clientel")
     */
    public function clientelAction()
    {
        return $this->render('home-clientel.html.twig');
    }

    /**
     * @Route("/home-stock", name="stock")
     */
    public function stocksAction()
    {
        return $this->render('stocks/index.html.twig');
    }

}
