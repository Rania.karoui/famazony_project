<?php

namespace App\DataFixtures;

use App\Entity\Cart;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CartFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $titles = [
          "Cart n°1",
          "Cart n°2",
          "Cart n°3",
          "Cart n°4"
        ];

        foreach ($titles as $title){
            $title = new Cart();
            $cart->setName($title);
            $manager->persist($cart);
        }

        $manager->flush();
    }
}
