<?php

namespace App\DataFixtures;

use App\Entity\Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ItemFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $items = [
            ["item 1","Description 1",30,60],
            ["item 2","Description 2",40, 80]
            
            
          ];


        foreach ($items as $itemInfos){
            $item = new Item();
            $item->setName($itemInfos[0]);
            $item->setDescription($itemInfos[1]);
            $item->setStock($itemInfos[2]);
            $item->setUnitPrice($itemInfos[3]);

            $manager->persist($item);
        }

        $manager->flush();
    }
}
