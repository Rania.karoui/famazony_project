<?php

namespace App\DataFixtures;

use App\Entity\Member;
use App\Repository\FamilyRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class MemberFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $members = [
          ["Jean","jean@test.com"],
          ["Alfred","alfred@test.com"],
        ];


        foreach ($members as $memberInfos){
            $member = new Member();
            $member->setName($memberInfos[0]);
            $member->setEmail($memberInfos[1]);

            $manager->persist($member);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
          FamilyFixtures::class
        ];
    }
}
